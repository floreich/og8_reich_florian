public class Addon {

    private String name;
    private float price;
    private int id, stock, maxStock;


    protected Addon(String name, float price, int id, int stock, int maxStock) {
        this.name = name;
        this.price = price;
        this.id = id;
        this.stock = stock;
        this.maxStock = maxStock;

        System.out.println();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getMaxStock() {
        return maxStock;
    }

    public void setMaxStock(int maxStock) {
        this.maxStock = maxStock;
    }

    public void consume() {}

    public void buy() {}

    public void currentStock() {}




}
