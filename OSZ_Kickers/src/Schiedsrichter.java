public class Schiedsrichter  {

    private String name;
    private int anzGepfiffeSpiele;


    public Schiedsrichter(String name, int anzGepfiffeSpiele) {
        this.name = name;
        this.anzGepfiffeSpiele = anzGepfiffeSpiele;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAnzGepfiffeSpiele() {
        return anzGepfiffeSpiele;
    }

    public void setAnzGepfiffeSpiele(int anzGepfiffeSpiele) {
        this.anzGepfiffeSpiele = anzGepfiffeSpiele;
    }

}
