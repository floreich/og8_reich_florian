public class Mitglied  {

    private String name;
    private String tel;
    private boolean jahresbeitragbezahlt;

    public Mitglied(String name, String tel, boolean jahresbeitragbezahlt) {
        this.name = name;
        this.tel = tel;
        this.jahresbeitragbezahlt = jahresbeitragbezahlt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}
