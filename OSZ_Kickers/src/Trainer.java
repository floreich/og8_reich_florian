import java.lang.reflect.Array;

public class Trainer extends Mitglied {

    private double aufwandsentschädigung;
    private char lizens;

    public Trainer(String name, String tel, boolean jahresbeitragbezahlt, char lizens, double aufwandsentschädigung) {
        super(name, tel, jahresbeitragbezahlt);
        this.lizens = lizens;
        this.aufwandsentschädigung = aufwandsentschädigung;
    }

    public double getAufwandsentschädigung() {
        return aufwandsentschädigung;
    }

    public void setAufwandsentschädigung(double aufwandsentschädigung) {
        this.aufwandsentschädigung = aufwandsentschädigung;
    }

    public char getLizens() {
        return lizens;
    }

    public void setLizens(char lizens) {
        this.lizens = lizens;
    }
}
